<?php
/**
 * Implements hook_permission().
 */
function rss_campaign_permission() {
  return array(
    'administer rss campaigns' => array(
      'title' => t('Administer RSS Campaigns'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu()
 */
function rss_campaign_menu() {
  $items = array();

  $items['admin/config/search/rss_campaign'] = array(
    'title' => t('RSS Campaign'),
    'access arguments' => array('administer rss campaigns'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rss_campaign_admin_form'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

function rss_campaign_admin_form($form, &$form_state) {
  $form['rss_campaign'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign to add to RSS feeds'),
    '#description' => t('Whatever you specify here will be included in the ref= parameter in all links from RSS feeds on the site.  Google Analytics can use this for tracking the origin of clicks. <em>If you leave this field blank, no campaign will be added to links.</em>'),
    '#default_value' => variable_get('rss_campaign', ''),
  );
  $form['rss_campaign_param'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameter name'),
    '#description' => t('The URL parameter to use.  Defaults to "ref"'),
    '#default_value' => variable_get('rss_campaign_param', 'ref'),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_node_view().
 *
 * @param $node
 * @param $op
 */
function rss_campaign_node_view($node, $view_mode) {
  if ($view_mode == 'rss') {
    // Remember that we're in RSS mode
    _rss_campaign_is_rss(TRUE);

    // Recreate the link to include the campaign
    $node->link = url('node/' . $node->nid, array('absolute' => TRUE));
  }
}

/**
 * Stores a static variable indicating whether or not RSS is currently being rendered.
 *
 * @param bool $is_rss If set, this value will be remembered.
 * @return bool The value that was previously set.
 */
function _rss_campaign_is_rss($is_rss = NULL) {
  $static = &drupal_static(__FUNCTION__, FALSE);
  if (isset($is_rss)) {
    $static = (bool)$is_rss;
  }
  return $static;
}

/**
 * Adds a campaign parameter to all outgoing URLs for RSS feeds.
 */
function rss_campaign_url_outbound_alter(&$path, &$options, $original_path) {
  if (_rss_campaign_is_rss()) {
    $campaign = variable_get('rss_campaign', '');
    $param = variable_get('rss_campaign_param', 'ref');

    if (!empty($campaign)) {
      $options['query'][$param] = $campaign;
    }
  }
}